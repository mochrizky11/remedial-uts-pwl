<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Kartu */

$this->title = 'Update Kartu: ' . $model->idkartu;
$this->params['breadcrumbs'][] = ['label' => 'Kartus', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idkartu, 'url' => ['view', 'id' => $model->idkartu]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kartu-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
