<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kartu".
 *
 * @property int $idkartu
 * @property string $kode
 * @property string|null $nama
 * @property float|null $iuran
 * @property float|null $diskon
 *
 * @property Customer[] $customers
 */
class Kartu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kartu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode'], 'required'],
            [['iuran', 'diskon'], 'number'],
            [['kode'], 'string', 'max' => 4],
            [['nama'], 'string', 'max' => 45],
            [['kode'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idkartu' => 'Idkartu',
            'kode' => 'Kode',
            'nama' => 'Nama',
            'iuran' => 'Iuran',
            'diskon' => 'Diskon',
        ];
    }

    /**
     * Gets query for [[Customers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCustomers()
    {
        return $this->hasMany(Customer::className(), ['idkartu' => 'idkartu']);
    }
}
